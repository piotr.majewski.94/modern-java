package pl.pmajewski.modernjava.repository;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import pl.pmajewski.modernjava.model.Transaction;

public class TransactionRepository implements Repository<Transaction> {

	private Set<Transaction> storage = new HashSet<>();
	
	@Override
	public Stream<Transaction> all() {
		return storage.stream();
	}

	@Override
	public Transaction add(Transaction item) {
		Long max = all().max(Comparator.comparing(Transaction::getId)).orElse(new Transaction(-1l)).getId();
		item.setId(++max);
		storage.add(item);
		return item;
	}
}
