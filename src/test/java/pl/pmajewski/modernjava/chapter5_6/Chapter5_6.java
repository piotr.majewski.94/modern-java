package pl.pmajewski.modernjava.chapter5_6;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Chapter5_6 {

	Trader raoul, mario, alan, brian;
	List<Transaction> transactions;
	
	@BeforeEach
	public void init() {
		
		raoul = new Trader("Raoul", "Cambridge");
		mario = new Trader("Mario","Milan");
		alan = new Trader("Alan","Cambridge");
		brian = new Trader("Brian","Cambridge");
		
		this.transactions = Arrays.asList(
				 new Transaction(brian, 2011, 300),
				 new Transaction(raoul, 2012, 1000),
				 new Transaction(raoul, 2011, 400),
				 new Transaction(mario, 2012, 710),
				 new Transaction(mario, 2012, 700),
				 new Transaction(alan, 2012, 950)
				);
	}
	
	
	@Test
	public void in2011() {
		List<Transaction> result = transactions.stream()
			.filter(i -> i.getYear() == 2011)
			.sorted(Comparator.comparing(Transaction::getValue))
			.collect(Collectors.toList());
		System.out.println("in2011 -> "+result);
	}
	
	@Test
	public void uniqueCities() {
		List<String> result = transactions.stream()
			.map(Transaction::getTrader)
			.map(Trader::getCity)
			.distinct()
			.collect(Collectors.toList());
		System.out.println("uniqueCities -> "+result);
	}
}
