package pl.pmajewski.modernjava.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Mapping {

	private List<String> data;
	
	@BeforeEach
	public void init() {
		data = Arrays.asList("Jonathan Archer", "T'Pol", "Charles Tucker", "Malcolm Reed", "Hoshi Sato", "Travis Mayweather", "Phlox", "Soval");
	}
	
	@Test
	public void numberOfCharacters() {
		List<Integer> result = data.stream()
			.map(String::length)
			.collect(Collectors.toList());
		System.out.println("numberOfCharacter -> "+result);
	}
	
	@Test
	public void uniqueCharacters() {
		List<String> result = data.stream()
			.map(i -> i.split(""))
			.flatMap(Arrays::stream)
			.map(String::trim)
			.map(String::toLowerCase)
			.filter(i -> i == null || !i.isEmpty())
			.distinct()
			.sorted()
			.collect(Collectors.toList());
		System.out.println("uniqueCharacters -> "+result);
	}
	
	@Test
	public void pairElements() {
		List<Integer> listA = Arrays.asList(1, 2 , 3);
		List<Integer> listB = Arrays.asList(4, 5, 6);
		
		List<String> result = listA.stream()
			.flatMap(i -> {
				return listB.stream()
					.map(j -> "("+i+", "+j+")");
			})
			.collect(Collectors.toList());
		System.out.println("pairElements -> "+result);
	}
}
