package pl.pmajewski.modernjava.streams;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Reducing {

	private List<Integer> data;
	
	@BeforeEach
	public void init() {
		data = Arrays.asList(1, 2, 3, 4, 1, 2, 3, 4, 5, 6, 7);
	}
	
	@Test
	public void max1() {
		Integer result = data.stream()
			.reduce(Integer::max)
			.get();
		System.out.println("max1 -> "+result);
	}
	
	@Test
	public void max2() {
		Integer result = data.stream()
			.reduce((i, j ) -> i > j ? i : j)
			.get();
		System.out.println("max2 -> "+result);
	}
	
}
