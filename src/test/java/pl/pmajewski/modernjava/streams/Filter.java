package pl.pmajewski.modernjava.streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Filter {
	
	private List<Integer> data;
	
	@BeforeEach
	public void init() {
		data = Arrays.asList(1, 2, 3, 1, 2, 3, 1, 2, 3, 4, 5, 6, 7);
	}
	
	@Test
	public void filter() {
		List<Integer> result = data.stream()
			.filter(i -> i < 4)
			.collect(Collectors.toList());
		System.out.println("Filter -> "+result);
	}
	
	@Test
	public void unique() {
		List<Integer> result = data.stream()
			.filter(i -> i < 4)
			.distinct()
			.collect(Collectors.toList());
		System.out.println("Unique -> "+result);
	}
	
	@Test
	public void sliceTakeDown() {
		List<Integer> result = data.stream()
			.sorted()
			.distinct()
			.takeWhile(i -> i < 4)
			.collect(Collectors.toList());
		System.out.println("TakeDown -> "+result);
	}
	
	@Test
	public void sliceDropWhile() {
		List<Integer> result = data.stream()
			.sorted()
			.distinct()
			.dropWhile(i -> i < 3 || i > 4)
			.collect(Collectors.toList());
		System.out.println("DropWhile -> "+result);
	}
	
	@Test
	public void limitMax3() {
		List<Integer> result = data.stream()
			.sorted(Collections.reverseOrder())
			.distinct()
			.limit(3)
			.collect(Collectors.toList());
		System.out.println("Max 3 -> "+result);
	}
	
	@Test
	public void skip3() {
		List<Integer> result = data.stream()
			.sorted()
			.distinct()
			.skip(3)
			.collect(Collectors.toList());
		System.out.println("Skip 3 first -> "+result);
	}
}
