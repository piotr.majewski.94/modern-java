package pl.pmajewski.modernjava.concurrency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompletableFutureTest {

	private List<Integer> iterations;
	
	private Long begin;
	private Long end;
	
	@BeforeEach
	public void init() {
		iterations = new ArrayList<>();
		for(int i=0; i<100; i++) {
			iterations.add(i);
		}
	}
	
	@BeforeEach
	private void start() {
		begin = System.currentTimeMillis();
	}
	
	@AfterEach
	private void end() {
		end = System.currentTimeMillis();
		System.out.println("Time: "+(end-begin)+" ms");
		System.out.println("====----====----====----====");
	}
	
	@Test
	public void test1() {
		iterations.parallelStream()
			.map(this::getPrice)
			.map(this::parse)
			.map(this::discount)
			.forEach(System.out::println);
	}
	
	@Test
	public void test2() {
		ExecutorService ex = Executors.newFixedThreadPool(100);
		List<CompletableFuture> toDo = new ArrayList<>();
		for(int i=0; i<iterations.size(); i++) {
			CompletableFuture<Double> f = CompletableFuture.supplyAsync(this::getPrice, ex)
					.thenApplyAsync(this::parse, ex)
					.thenApplyAsync(this::discount, ex);
			toDo.add(f);
		}
		toDo.stream()
			.map(CompletableFuture::join);
		toDo.stream()
			.forEach(i -> {
				try {
					System.out.println(i.get());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});

	}
	
	private void dealy() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private String getPrice(Integer fake) {
		dealy();
		Random rand = new Random();
		Double value = rand.nextDouble()*100d;
		return value.toString();
	}
	
	private String getPrice() {
		dealy();
		Random rand = new Random();
		Double value = rand.nextDouble()*100d;
		return value.toString();
	}
	
	private double parse(String value) {
		return Double.parseDouble(value);
	}
	
	private Double discount(Double value) {
		dealy();
		Random rand = new Random();
		return 0.75d + (1.0d  - 0.75d) * rand.nextDouble(); 
	}
	
}
