package pl.pmajewski.modernjava.collections;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CollectionTest {

	@Test
	public void test() {
		Assertions.assertThrows(UnsupportedOperationException.class, () -> {
			List<String> list = Arrays.asList("Jean-Luc");
			list.add("Picard");
		});
	}
	
	@Test
	public void test2() {
		List<String> list = List.of("Jean-Luc");
		list.add("Picard");
		System.out.println(list);
	}
	
	@Test
	public void test3() {
		Map<String, Integer> map = Map.of("P", 25, "M", 23);
		map.compute(key, remappingFunction)
	}
}
